# auto-switch-tab
基于`@better-scroll/core`实现的自动循环切换tab组件

#### 安装
```bash
npm install auto-switch-tab --save
```

ps: 该组件会获取父节点的宽度进行计算，所以在使用时请主要父节点元素的宽度是否正常（如你想的那样）！

#### 参数
| 参数名 | 默认值 | 数据类型 | 说明|
|--|--|--|--|
| tabList | [ {name: 'xx', ...} ] | Array[Object] | 数据源（name字段必须）  |
| defaultActive| 0 | Number | 默认激活的tab索引 |
| intervalTime| 5000 | Number  | tab自动切换间隔时间 |


#### 全局注册
`main.js` 添加如下代码：
```javascript
import AutoSwitchTab from 'auto-switch-tab'
Vue.use(AutoSwitchTab)
```

#### 局部注册（页面使用）
```javascript
<script>
import { AutoSwitchTab } from 'auto-switch-tab'

export default {
  name: 'App',
  components: {
    AutoSwitchTab
  }
}
</script>
```

#### 使用
```vue
<template>
  <div id="app">
    <AutoSwitchTab :tab-list="tabsList"></AutoSwitchTab>
  </div>
</template>

<script>
import { AutoSwitchTab } from 'auto-switch-tab'

export default {
  name: 'App',
  components: {
    AutoSwitchTab
  },
  data() {
    return {
      tabsList: [
          {
            name: '自动滚动tab',
            value: 1
          },
          {
            name: '哈哈嘻嘻哈哈嘻嘻哈哈嘻嘻',
            value: 2
          },
          {
            name: '默认没5s切换一次',
            value: 3
          },
          {
            name: 'tab4',
            value: 4
          },
          {
            name: 'tab5',
            value: 5
          },
          {
            name: 'tab6',
            value: 6
          },
          {
            name: 'tab7',
            value: 7
          },
          {
            name: 'tab8',
            value: 8
          },
          {
            name: 'tab9',
            value: 9
          }
        ]
    }
  }
}
</script>
```

#### 效果
![组件效果](https://i.postimg.cc/nL0n5Jm9/auto-switch-tab-demo.gif)
