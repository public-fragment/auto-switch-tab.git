// 1. 引入组件
import AutoSwitchTab from './autoSwitchTab';

// 3. 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install = function (Vue) {
  // 判断是否安装
  if (install.installed) return;
  install.installed = true; //标识已经安装
  // 注册全局组件
  Vue.component(AutoSwitchTab.name, AutoSwitchTab);
}

// 对于那些没有在应用中使用模块化系统的用户（如在html中直接使用vue），他们往往将通过 <script> 标签引用你的插件，并期待插件无需调用 Vue.use() 便会自动安装 
// 添加如下几行代码来实现自动安装：
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
};


// 4. 导出组件
// 将每个组件导出，便于不全局注册时，单个使用component注册为页面级组件
export {
  AutoSwitchTab
};

// 导出install方法，用于vue.use 注册全局插件
export default install;